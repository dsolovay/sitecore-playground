﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pipelines.HttpRequest;
namespace SitecorePlayground.Diagnostics
{
    public class HttpPipelineLogger : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            Sitecore.Diagnostics.Log.Debug(message: Sitecore.Context.Database.Name);
        }

       }
}