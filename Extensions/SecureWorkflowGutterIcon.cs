﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitecorePlayground.Extensions
{
    public class SecureWorkflowGutterIcon : Sitecore.Shell.Applications.ContentEditor.Gutters.WorkflowState
    {
        protected override Sitecore.Shell.Applications.ContentEditor.Gutters.GutterIconDescriptor GetIconDescriptor(Sitecore.Data.Items.Item item)
        {
            if (Sitecore.Context.Item.Locking.IsLocked() == false || 
                Sitecore.Context.Item.Locking.GetOwner() == Sitecore.Context.GetUserName())
            {
                return base.GetIconDescriptor(item);
            }
            return null;
        }
    }
}