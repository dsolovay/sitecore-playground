﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Links;

namespace SitecorePlayground.WebControls
{
    public class ShowChildLinks : Sitecore.Web.UI.WebControl
    {

        protected override string GetCachingID()
        {
            return this.GetType().Name + (IsAlias() ? "Alias": "Item");
        }

        private bool IsAlias()
        {
            return Sitecore.Context.Database.Aliases.Exists(Sitecore.Context.RawUrl);
        }

        protected override void DoRender(HtmlTextWriter output)
        {
            var childItems = Sitecore.Context.Item.GetChildren();
            foreach (var child in childItems.InnerChildren)
            {
                output.RenderBeginTag("UL");
                if (HasPresentation(child))
                {
                    output.RenderBeginTag("LI");
                    output.Write(LinkManager.GetItemUrl(child));
                    output.RenderEndTag();
                }
                output.RenderEndTag();
            }
            WriteParagraph(output, "Raw URL:" + Sitecore.Context.RawUrl);
            WriteParagraph(output, "Rendered at " + DateTime.Now.ToLongTimeString());
        }

        private static void WriteParagraph(HtmlTextWriter output, string content)
        {
            output.RenderBeginTag(HtmlTextWriterTag.P);
            output.Write(content);
            output.RenderEndTag();
        }
        
        private bool HasPresentation(Item item)
        {
            return item["__Renderings"] != "";
        }
    }
}