﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Shell.Framework.Commands;

namespace SitecorePlayground.Extensions
{
    public class TestCommand : Command
    {
        public override void Execute(CommandContext context)
        {
            Item[] items = context.Items;
            Item first = items[0];
            Sitecore.Diagnostics.Log.Info(String.Format("{0} item selected, first is {1}",
                items.Count(), first != null ? first.Name : "<none>"),this);
            var publishCommand = new PublishItem();
            publishCommand.Execute(context);

        }
    }
}