﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Pipelines.HttpRequest;

namespace SitecorePlayground.Pipelines
{
    public class SiteChanger : HttpRequestProcessor
    {
        /// <summary>
        /// List of sites that need to have targets changed.
        /// </summary>
        private readonly string[] _targetSites = {"demo_site2"};

        private const string SOURCE_SITE_ROOT = "/sitecore/content/Home/";
        private const string TARGET_SITE_ROOT = "/sitecore/content/AlternateSite/";

        public override void Process(HttpRequestArgs args)
        {

            if (isReferringSiteInTargetList())
            {
                Item targetItem = getTargetItemOrNullIfNotFound();
                if (targetItem != null)
                {
                    Sitecore.Context.Item = targetItem;
                }
            }
        }

        private bool isReferringSiteInTargetList()
        {
            Uri referrer = HttpContext.Current.Request.UrlReferrer;
            if (referrer != null)
            {
                return _targetSites.Contains(referrer.Host);
            }
            return false;
        }

        private Item getTargetItemOrNullIfNotFound()
        {
            if (Sitecore.Context.Item != null)
            {
                string currentItemPath = Sitecore.Context.Item.Paths.FullPath;
                string newPath;

                if (currentItemPath.Contains(SOURCE_SITE_ROOT))
                {
                    newPath = currentItemPath.Replace(SOURCE_SITE_ROOT, TARGET_SITE_ROOT);
                    return Sitecore.Context.Database.GetItem(newPath);
                }
            }
            return null;
        }
    }
}