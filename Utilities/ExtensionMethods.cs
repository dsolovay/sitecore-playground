﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Sites;
using SitecorePlayground.WebControls;

namespace SitecorePlayground.Utilities
{
    public static class ExtensionMethods
    {
        public static Item GetHomeItem(this SiteContext context)
        {
            return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
        }

        public static IEnumerable<Item> GetSiteSections(this SiteContext context)
        {
            Item homeNode = context.GetHomeItem();
            var returnList = new List<Item>();
            foreach (Item child in homeNode.GetChildren().InnerChildren)
            {
                if (child.HasPresentation())
                {
                    returnList.Add(child);
                }
            }
            return returnList;
        }
    }
}