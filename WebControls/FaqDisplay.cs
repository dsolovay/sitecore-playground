﻿using System;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Web.UI;
using Sitecore.Web.UI.WebControls;

namespace SitecorePlayground.WebControls
{
    public class FaqDisplay : Sitecore.Web.UI.WebControl
    {
        protected override void DoRender(HtmlTextWriter output)
        {
            output.RenderBeginTag("UL");
            foreach (var faqItem in Sitecore.Context.Item.GetChildren().InnerChildren)
            {
                if (SiteFieldMatcesContextSiteOrIsBlank(faqItem))
                {
                    output.RenderBeginTag("LI");
                    output.RenderBeginTag("H2");
                    output.Write(faqItem["Question"]);
                    output.RenderEndTag();
                    output.RenderBeginTag("H4");
                    output.Write(faqItem["Answer"]);
                    output.RenderEndTag();

                }
            }
            output.RenderEndTag();
        }

        private static bool SiteFieldMatcesContextSiteOrIsBlank(Item item)
        {
            return 
                item["Site"] == string.Empty || 
                string.Equals(item["Site"], Sitecore.Context.Site.Name, 
                StringComparison.OrdinalIgnoreCase);
        }
    }
}