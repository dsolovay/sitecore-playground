﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace SitecorePlayground.WebControls
{
    public class ShowContextControl : Sitecore.Web.UI.WebControl
    {
        private HtmlTextWriter _output;
        private const HtmlTextWriterTag P = HtmlTextWriterTag.P;
        private const HtmlTextWriterTag H2 = HtmlTextWriterTag.H2;

        protected override void DoRender(System.Web.UI.HtmlTextWriter output)
        {
            _output = output;
           
            var siteName = Sitecore.Context.GetSiteName();
            var itemPath = Sitecore.Context.Item.Paths.ContentPath;

            output.BeginRender();
            RenderOutputElement(H2, "Context Attributes");
            RenderOutputElement(P,String.Format("SiteName is {0}", siteName));
            RenderOutputElement(P,String.Format("ItemPath is {0}", itemPath));
            output.EndRender();
        }

        private void RenderOutputElement(HtmlTextWriterTag tag, string content)
        {
            _output.RenderBeginTag(tag);
            _output.Write(content);
            _output.RenderEndTag();
        }
    }
}