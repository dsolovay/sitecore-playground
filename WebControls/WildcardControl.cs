﻿using System.Collections.Generic;
using System.Web.UI;
using Sitecore.Data.Items;
using Sitecore.Web;
using Sitecore.Web.UI;
using Sitecore.Web.UI.WebControls;
using SitecorePlayground.Utilities;

namespace SitecorePlayground.WebControls
{
    public class WildcardControl : WebControl
    {
        protected override void DoRender(HtmlTextWriter output)
        {

            string rawUrl = Sitecore.Context.RawUrl;
            const int lastValue = 0;
            string itemName = WebUtil.GetUrlName(lastValue);

            output.BeginRender();
            WriteDiagnostics(rawUrl, itemName, output);

            Item foundItem = FindMatchingItem(itemName);
            if (foundItem != null)
            {
                PrintParagraph(output, "Found match for " + itemName);
                RenderItem(foundItem, output);
            }
            else
            {
                PrintParagraph(output, "No match found for " + itemName);
            }

            PrintBackLink(output);
            output.EndRender();
        }

        private static Item FindMatchingItem(string itemName)
        {
            IEnumerable<Item> siteSections = Sitecore.Context.Site.GetSiteSections();
            return GetFirstMatchingItem(itemName, siteSections);
        }

        private static Item GetFirstMatchingItem(string itemName, IEnumerable<Item> searchLocations)
        {
            foreach (Item location in searchLocations)
            {
                Item foundItem = Sitecore.Context.Database.GetItem(location.Paths.FullPath + "/" + itemName);
                if (foundItem != null && foundItem.Name != "*")  //Prevent matching wildcard itself!
                {
                    return foundItem;
                }
            }
            return null;
        }
        
        private static void RenderItem(Item foundItem, HtmlTextWriter output)
        {
            var xsl = new XslFile();
            var rendering =
                Sitecore.Context.Database.GetItem("/sitecore/layout/Renderings/Starter Kit/Page Title and Text");
            xsl.Path = rendering["Path"];
            xsl.DataSource = foundItem.Paths.FullPath;
            string result = xsl.RenderAsText();
            output.Write(result);
        }

        private static void WriteDiagnostics(string rawUrl, string itemName, HtmlTextWriter output)
        {
            PrintParagraph(output, "Raw URL: " + rawUrl);
            PrintParagraph(output, "Item Name: " + itemName);
        }

        private static void PrintBackLink(HtmlTextWriter output)
        {
            output.AddAttribute("href", "/wildcards");
            output.RenderBeginTag("A");
            output.Write("Return to Wildcards Page");
            output.RenderEndTag();
        }
       
        private static void PrintParagraph(HtmlTextWriter output, string text)
        {
            output.RenderBeginTag("P");
            output.Write(text);
            output.RenderEndTag();
        }
    }

    static class LocalExtensions
    {
        public static bool HasPresentation(this Item item)
        {
            return item.Visualization.GetLayout(Sitecore.Context.Device) != null;
        }
    }

}