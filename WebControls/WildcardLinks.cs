﻿using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using Sitecore.Web.UI;
using Sitecore.Data.Items;
using SitecorePlayground.Utilities;

namespace SitecorePlayground.WebControls
{
    public class WildcardLinks : WebControl
    {
        protected override void DoRender(HtmlTextWriter output)
        {
            var items = GetLinkItems();
            output.RenderBeginTag("UL");
            foreach (var item in items)
            {
                output.RenderBeginTag("LI");
                CreateLink(output, item);
                output.RenderEndTag();
            }
            output.RenderEndTag();

        }

        private void CreateLink(HtmlTextWriter output, Sitecore.Data.Items.Item item)
        {
           
            output.AddAttribute("href", "/wildcards/" + item.Name);
            output.RenderBeginTag("A");
            output.Write(item.Name);
            output.RenderEndTag();
        }


        private IEnumerable<Item> GetLinkItems()
        {
            List<Item> returnList = new List<Item>();
            foreach (var siteSection in Sitecore.Context.Site.GetSiteSections())
            {
                returnList.AddRange(siteSection.GetChildren());
                
            }
            return returnList.FindAll(item => item.Name != "*");
        }
    }
}