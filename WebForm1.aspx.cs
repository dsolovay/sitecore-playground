﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace SitecorePlayground
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
             Sitecore.Data.Database master = Sitecore.Data.Database.GetDatabase("master");
            Item item = master.GetItem(new ID(this.Guid.Text.ToString()));
            var field = item.Fields[Field.Text.ToString()];
            
            Label1.Text =
                string.Format("Item {0}, field {1},  ContainsStandardValue {2}, InheritsValueFromOtherItem {3}<br />",
                              item.Name, field.Name, field.ContainsStandardValue, field.InheritsValueFromOtherItem);

        }
    }
}